const Users = [
  {
    id: 1,
    email: '1@gmail.com',
    userName: 'user1',
    password: 'password',
    userToken: 'token123',
  },
  {
    id: 2,
    email: '2@gmail.com',
    userName: 'user2',
    password: 'password',
    userToken: 'token321',
  },
  {
    id: 3,
    email: '3@gmail.com',
    userName: 'user3',
    password: 'password',
    userToken: 'token101112',
  },
];

export default Users;
