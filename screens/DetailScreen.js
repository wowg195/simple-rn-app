import React from 'react';
import {View, Text, Button} from 'react-native';

const DetailScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Detail Screen</Text>
      {/* <Button title="Push" onPress={() => navigation.push('Detail')} />
      <Button title="Navigate" onPress={() => navigation.navigate('Home')} /> */}
    </View>
  );
};

export default DetailScreen;
